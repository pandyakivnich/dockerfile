FROM debian:9 as build

RUN apt update && apt upgrade -y && apt install -y vim wget gcc make libpcre++-dev
RUN wget 'https://github.com/openresty/lua-nginx-module/archive/v0.10.19.tar.gz' && wget 'https://github.com/vision5/ngx_devel_kit/archive/v0.3.1.tar.gz' && wget 'https://github.com/openresty/luajit2/archive/v2.1-20201027.tar.gz' && wget 'openresty.org/download/nginx-1.19.3.tar.gz'
RUN tar -xzvf v0.10.19.tar.gz && tar -xzvf v0.3.1.tar.gz && tar -xzvf v2.1-20201027.tar.gz && tar -xzvf nginx-1.19.3.tar.gz
RUN cd /luajit2-2.1-20201027/ && make && make install && cd /nginx-1.19.3/ && export LUAJIT_LIB=/usr/local/lib && export LUAJIT_INC=/usr/local/include/luajit-2.1 && ./configure --prefix=/opt/nginx --with-ld-opt="-Wl,-rpath,$LUAJIT_LIB" --add-dynamic-module=/ngx_devel_kit-0.3.1 --add-dynamic-module=/lua-nginx-module-0.10.19 --without-http_gzip_module && make && make install

FROM debian:9

WORKDIR /usr/local/lib
COPY --from=build /usr/local/lib .
WORKDIR /opt/nginx/sbin/
COPY --from=build /opt/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
